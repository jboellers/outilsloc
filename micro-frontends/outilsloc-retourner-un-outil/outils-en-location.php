<?php
error_reporting(E_ALL);

require_once(dirname(__FILE__)."/OutilEnLocation.php");

function creerConnexion() {
    $servername = "outilsloc-retourner-un-outil-db";
    $username = "root";
    $password = "";
    $dbname = "outilsloc";

    $connexion = new mysqli($servername, $username, $password, $dbname);

    if ($connexion->connect_error) {
        http_response_code(500);
        die();
    }
    $connexion->set_charset("utf8");
    return $connexion;
}

function construireRequete($connexion) {
    $requeteSQL = "SELECT description, image, poids FROM outil_en_location WHERE 1=1 ";

    if (isset($_GET['enLocationChez']))
        $requeteSQL .= "AND enLocationChez = ? ";
    
    $requetePreparee = $connexion->prepare($requeteSQL);
    
    if (isset($_GET['enLocationChez']))
        $requetePreparee->bind_param('s', $_GET['enLocationChez']);

    return $requetePreparee;
}

$connexion = creerConnexion();

$requetePreparee = construireRequete($connexion);

$requetePreparee->execute();
$resultat = $requetePreparee->get_result();

$outilsEnLocation = array();
while($ligne = $resultat->fetch_assoc()) {
    $outilsEnLocation[] = new OutilEnLocation("data:image/jpeg;base64," . $ligne["image"], $ligne["description"], $ligne["poids"]);
}

$requetePreparee->close();
$connexion->close();

header('Content-type: application/json');
$listeOutils = new stdClass();
$listeOutils->outilsEnLocation = $outilsEnLocation;
echo json_encode($listeOutils);

?>

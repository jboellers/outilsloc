export default function () {

    return {
        recupererOutilsEnLocation: async (filtre) => {
            const response = await fetch('./retourner-un-outil/outils-en-location.php?' + filtre);
            const listeOutils = await response.json();
            return listeOutils.outilsEnLocation;
        }
    };
}

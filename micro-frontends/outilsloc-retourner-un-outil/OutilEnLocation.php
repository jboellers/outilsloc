<?php

final class OutilEnLocation {
    public $image;
    public $description;
    public $poids;

    function __construct(string $image, string $description, string $poids) {
        $this->image = $image;
        $this->description = $description;
        $this->poids = $poids;
    }
}

export default function () {

    return {
        recupererOutilsDisponibles: async () => {
            const response = await fetch('./louer-un-outil/outils-disponibles.php');
            const listeOutils = await response.json();
            return listeOutils.outilsDisponibles;
        }
    };
}

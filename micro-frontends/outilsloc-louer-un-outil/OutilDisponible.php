<?php

final class OutilDisponible {
    public $image;
    public $description;
    public $prix;

    function __construct(string $image, string $description, string $prix) {
        $this->image = $image;
        $this->description = $description;
        $this->prix = $prix;
    }
}

<?php
error_reporting(E_ALL);

require_once(dirname(__FILE__)."/OutilDisponible.php");

function creerConnexion() {
    $servername = "outilsloc-louer-un-outil-db";
    $username = "root";
    $password = "";
    $dbname = "outilsloc";

    $connexion = new mysqli($servername, $username, $password, $dbname);

    if ($connexion->connect_error) {
        http_response_code(500);
        die();
    }
    $connexion->set_charset("utf8");
    return $connexion;
}

$connexion = creerConnexion();

$requetePreparee = $connexion->prepare("SELECT description, image, prix FROM outil_disponible");

$requetePreparee->execute();
$resultat = $requetePreparee->get_result();

$outilsDisponibles = array();
while($ligne = $resultat->fetch_assoc()) {
    $outilsDisponibles[] = new OutilDisponible("data:image/jpeg;base64," . $ligne["image"], $ligne["description"], $ligne["prix"]);
}

$requetePreparee->close();
$connexion->close();

header('Content-type: application/json');
$listeOutils = new stdClass();
$listeOutils->outilsDisponibles = $outilsDisponibles;
echo json_encode($listeOutils);

?>

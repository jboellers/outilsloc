# Outilsloc : partie Micro Frontends

Cette partie présente le mini-site de location d'outils structuré en deux Micro Frontends : un pour la page "Louer un outil" et l'autre pour la page "Retourner un outil".

## Structure

Le projet est découpé en trois répertoires correspondant chacun à une page du site :
- [outilsloc-armature](outilsloc-armature) : armature du site et module page d'accueil
- [outilsloc-louer-un-outil](outilsloc-louer-un-outil) : module de location d'un outil
- [outilsloc-retourner-un-outil](outilsloc-retourner-un-outil) : module de retour d'un outil

## Configuration

Pour faire fonctionner le site en local deux options sont documentées : avec ou sans Docker.

### Avec Docker et Docker Compose

Pour démarrer les cinq conteneurs (armature, louer un outil web et db, retourner un outil web et db) :
```bash
$ docker-compose up -d
```

Vous pouvez alors accéder au site sur http://localhost:81

Pour arrêter et supprimer les trois conteneurs :
```bash
$ docker-compose down
```

### Sans Docker

#### Module louer un outil

- Démarrer une base de données SQL en UTF-8
- Jouer le script SQL de création et d'alimentation présent dans [outilsloc-louer-un-outil](outilsloc-louer-un-outil)
- Démarrer un serveur web et php avec l'extension mysqli activée et servant le contenu du répertoire [outilsloc-louer-un-outil](outilsloc-louer-un-outil)
- Configurer la connexion à la BDD dans [outilsloc-louer-un-outil/outils-disponibles.php](outilsloc-louer-un-outil/outils-disponibles.php)

#### Module retourner un outil

- Démarrer une base de données SQL en UTF-8
- Jouer le script SQL de création et d'alimentation présent dans [outilsloc-retourner-un-outil](outilsloc-retourner-un-outil)
- Démarrer un serveur web et php avec l'extension mysqli activée et servant le contenu du répertoire [outilsloc-retourner-un-outil](outilsloc-retourner-un-outil)
- Configurer la connexion à la BDD dans [outilsloc-retourner-un-outil/outils-en-location.php](outilsloc-retourner-un-outil/outils-en-location.php)

#### Module armature

- Démarrer un serveur web servant le contenu du répertoire [outilsloc-armature](outilsloc-armature)
- Le configurer pour que les appels à /louer-un-outil redirigent vers le serveur du module louer-un-outil
- Le configurer pour que les appels à /retourner-un-outil redirigent vers le serveur du module retourner-un-outil

export default class PageAccueil extends HTMLElement {

    constructor() {
        super();

        const shadow = this.attachShadow({ mode: 'open' });

        shadow.innerHTML = `
            <style>
                button {
                    height: 100px;
                    width: 200px;
                    font-size: 20px;
                    display: block;
                    margin-left: auto;
                    margin-right: auto;
                    margin-bottom: 28px;
                }
            </style>
            <div>
                <h1>Accueil</h1>
                <button onclick="window.location.hash = '#louer-un-outil'">Louer un outil</button>
                <button onclick="window.location.hash = '#retourner-un-outil'">Retourner un outil</button>
            </div>
        `;
    }
}

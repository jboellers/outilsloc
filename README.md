# Outilsloc : Micro Frontends et Web Components

Ce projet est une expérimentation mettant en œuvre des Micro Frontends grâce à des Web Components. La première partie (répertoire [web-components](web-components)) construit un mini-site de locations d'outils grâce à des Web Components étape par étape. La seconde partie (répertoire [micro-frontends](micro-frontends)) réorganise le site en deux Micro Frontends.

Le diaporama accompagnant le projet est disponible sur le [wiki du projet](https://gitlab.com/jboellers/outilsloc/-/wikis/home).

## License

outilsloc est sous license [GNU GPLv3](LICENSE)

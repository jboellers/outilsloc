import { OutilEnLocation } from './outil-en-location.js'
import outilsService from './outils-service.js';

export default class PageRetournerUnOutil extends HTMLElement {

    constructor() {
        super();

        const shadow = this.attachShadow({ mode: 'open' });

        if (undefined == customElements.get('app-outil-en-location')) {
            customElements.define('app-outil-en-location', OutilEnLocation);
        }

        shadow.innerHTML = `
            <style>
                app-outil-en-location {
                    margin: 0 10px 0 10px;
                }
            </style>
            <div class="page-retourner-un-outil">
                <h1>Retourner un outil</h1>
            </div>
        `;

        outilsService().recupererOutils('enLocationChez=lucien')
            .then(outils => {

                outils.forEach(outil => {
                    var outilElem = document.createElement('app-outil-en-location');
                    outilElem.setAttribute('image', outil.image);
                    outilElem.setAttribute('description', outil.description);
                    outilElem.setAttribute('poids', outil.poids);

                    this.shadowRoot.querySelector('.page-retourner-un-outil').appendChild(outilElem);
                });

            }).catch(() => {
                var erreur = document.createElement('p');
                erreur.innerText = 'Ce service est momentanément indisponible. Veuillez nous excuser pour la gêne occasionnée.';
                this.shadowRoot.querySelector('.page-retourner-un-outil').appendChild(erreur);
            });
    }
}

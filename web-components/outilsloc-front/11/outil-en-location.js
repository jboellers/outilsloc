export class OutilEnLocation extends HTMLElement {

    static get observedAttributes() {
        return ['image', 'description', 'poids'];
    }

    constructor() {
        super();

        const shadow = this.attachShadow({ mode: 'open' });

        shadow.innerHTML = `
            <style>
                .outil-en-location { 
                    max-width: 700px;
                    display: grid;
                    grid-template-columns: 1fr 1fr 1fr 1fr;
                    grid-gap: 10px;
                    
                    border: 1px solid #c8c8c8;
                    border-radius: 3px;
                    padding: 10px;
                }
                button {
                    grid-column: 1; grid-row: 1 / span 2;
                    font-size: 20px;
                }
                .description {
                    grid-column: 2 / span 2; grid-row: 1;
                    font-size: 25px;
                }
                .poids {
                    grid-column: 2 / span 2; grid-row: 2;
                    color: #707070;
                }
                img {
                    grid-column: 4; grid-row: 1 / span 2;
                    margin-left: auto;
                    margin-right: auto;
                }
            </style>
            <div class="outil-en-location">
                <button>Éditer le bon de retour</button>
                <span class="description"></span>
                <span class="poids"></span>
                <img></img>
            </div>
        `;
        this.image = shadow.querySelector('img');
        this.description = shadow.querySelector('.description');
        this.poids = shadow.querySelector('.poids');
    }

    attributeChangedCallback() {
        this.image.setAttribute('src', this.getAttribute('image'));
        this.description.textContent = this.getAttribute('description');
        this.poids.textContent = this.getAttribute('poids');
    }

}

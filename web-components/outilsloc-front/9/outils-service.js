export default function () {

    return {
        recupererOutils: async (filtre) => {
            const response = await fetch('/api/outils.php?' + filtre);
            const listeOutils = await response.json();
            return listeOutils.outils;
        }
    };
}

export class OutilDisponible extends HTMLElement {

    static get observedAttributes() {
        return ['description'];
    }

    constructor() {
        super();

        const composant = document.createElement('div');
        composant.innerHTML = `
            <style>
                .outil-disponible { 
                    max-width: 700px;
                    display: grid;
                    grid-template-columns: 1fr 1fr 1fr 1fr;
                    grid-gap: 10px;
                    
                    border: 1px solid #c8c8c8;
                    border-radius: 3px;
                    padding: 10px;
                }
                img {
                    grid-column: 1; grid-row: 1 / span 2;
                }
                .description {
                    grid-column: 2 / span 2; grid-row: 1;
                    font-size: 25px;
                }
                .prix {
                    grid-column: 2 / span 2; grid-row: 2;
                    color: #707070;
                }
                button {
                    grid-column: 4; grid-row: 1 / span 2;
                    font-size: 20px;
                }
            </style>
            <div class="outil-disponible">
                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAIAAAD/gAIDAAAAAXNSR0IArs4c6QAAAARnQU1BAACx
                    jwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAADPSURBVHhe7cdBCQAwDMDA+ZdRdZWxf2BhAgL3
                    uXNm84uP4CP4CD6Cj+Aj+Ag+go/gI/gIPoKP4CP4CD6Cj+Aj+Ag+go/gI/gIPoKP4CP4CD6Cj+Aj
                    +Ag+go/gI/gIPoKP4CP4CD6Cj+Aj+Ag+go/gI/gIPoKP4CP4CD6Cj+Aj+Ag+go/gI/gIPoKP4CP4
                    CD6Cj+Aj+Ag+go/gI/gIPoKP4CP4CD6Cj+Aj+Ag+go/gI/gIPoKP4CP4CD6Cj+Aj+Ag+go/gI/gI
                    PoKP4CP4vMxemwgiJZ7Z6q8AAAAASUVORK5CYII="></img>
                <span class="description"></span>
                <span class="prix">13€ par jour</span>
                <button>Commander</button>
            </div>
        `;

        this.description = composant.querySelector('.description');

        this.appendChild(composant);
    }

    attributeChangedCallback() {
        this.description.textContent = this.getAttribute('description');
    }

}
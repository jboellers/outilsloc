export class OutilDisponible extends HTMLElement {

    static get observedAttributes() {
        return ['image', 'description', 'prix'];
    }

    constructor() {
        super();

        const shadow = this.attachShadow({ mode: 'open' });
        shadow.innerHTML = `
            <style>
                .outil-disponible { 
                    max-width: 700px;
                    display: grid;
                    grid-template-columns: 1fr 1fr 1fr 1fr;
                    grid-gap: 10px;
                    
                    border: 1px solid #c8c8c8;
                    border-radius: 3px;
                    padding: 10px;
                }
                img {
                    grid-column: 1; grid-row: 1 / span 2;
                }
                .description {
                    grid-column: 2 / span 2; grid-row: 1;
                    font-size: 25px;
                }
                .prix {
                    grid-column: 2 / span 2; grid-row: 2;
                    color: #707070;
                }
                button {
                    grid-column: 4; grid-row: 1 / span 2;
                    font-size: 20px;
                }
            </style>
            <div class="outil-disponible">
                <img></img>
                <span class="description"></span>
                <span class="prix"></span>
                <button>Commander</button>
            </div>
        `;

        this.image = shadow.querySelector('img');
        this.description = shadow.querySelector('.description');
        this.prix = shadow.querySelector('.prix');
    }

    attributeChangedCallback() {
        this.image.setAttribute('src', this.getAttribute('image'));
        this.description.textContent = this.getAttribute('description');
        this.prix.textContent = this.getAttribute('prix');
    }

}
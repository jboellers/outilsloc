import { OutilDisponible } from './outil-disponible.js';
import outilsService from './outils-service.js';

export default class PageLouerUnOutil extends HTMLElement {

    constructor() {
        super();

        const shadow = this.attachShadow({ mode: 'open' });

        if (undefined == customElements.get('app-outil-disponible')) {
            customElements.define('app-outil-disponible', OutilDisponible);
        }

        shadow.innerHTML = `
            <style>
                app-outil-disponible {
                    margin: 0 10px 0 10px;
                }
            </style>
            <div class="page-louer-un-outil">
                <h1>Louer un outil</h1>
            </div>
        `;

        outilsService().recupererOutils('disponibleLocation=1')
            .then(outils => {

                outils.forEach(outil => {
                    var outilElem = document.createElement('app-outil-disponible');
                    outilElem.setAttribute('image', outil.image);
                    outilElem.setAttribute('description', outil.description);
                    outilElem.setAttribute('prix', outil.prix);

                    this.shadowRoot.querySelector('.page-louer-un-outil').appendChild(outilElem);
                });

            })
            .catch(() => {
                var erreur = document.createElement('p');
                erreur.innerText = 'Ce service est momentanément indisponible. Veuillez nous excuser pour la gêne occasionnée.';
                this.shadowRoot.querySelector('.page-louer-un-outil').appendChild(erreur);
            });
    }
}

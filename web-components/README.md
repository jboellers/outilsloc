# Outilsloc : partie Web Components

Cette partie présente le mini-site de location d'outils construit étape par étape en utilisant des Web Components.

## Structure

Le projet est découpé en trois répertoires :
- [outilsloc-db](outilsloc-db) : scripts de création et d'alimentation de la BDD
- [outilsloc-back](outilsloc-back) : scripts PHP d'accès à la BDD. Retournent du JSON
- [outilsloc-front](outilsloc-front) : étapes de constuction du frontend en Web Components

## Configuration

Pour faire fonctionner le site en local deux options sont documentées : avec ou sans Docker.

### Avec Docker et Docker Compose

Pour démarrer les trois conteneurs (front, back et db) :
```bash
$ docker-compose up -d
```

Vous pouvez alors accéder au site sur http://localhost/13

Pour arrêter et supprimer les trois conteneurs :
```bash
$ docker-compose down
```

### Sans Docker

- Démarrer une base de données SQL en UTF-8
- Jouer le script SQL de création et d'alimentation présent dans [outilsloc-db](outilsloc-db)
- Démarrer un serveur php avec l'extension mysqli activée et servant le contenu du répertoire [outilsloc-back](outilsloc-back)
- Configurer la connexion à la BDD dans [outilsloc-back/outils.php](outilsloc-back/outils.php)
- Démarrer un serveur web et servant le contenu du répertoire [outilsloc-front](outilsloc-front)
- Le configurer pour que les appels à /api redirigent vers le serveur php

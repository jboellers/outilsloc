<?php

final class Outil {
    public $image;
    public $description;
    public $prix;
    public $poids;

    function __construct(string $image, string $description, string $prix, string $poids) {
        $this->image = $image;
        $this->description = $description;
        $this->prix = $prix;
        $this->poids = $poids;
    }
}

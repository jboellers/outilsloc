<?php
error_reporting(E_ALL);

require_once(dirname(__FILE__)."/Outil.php");

function creerConnexion() {
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "outilsloc";

    $connexion = new mysqli($servername, $username, $password, $dbname);

    if ($connexion->connect_error) {
        http_response_code(500);
        die();
    }
    $connexion->set_charset("utf8");
    return $connexion;
}

function construireRequete($connexion) {
    $requeteSQL = "SELECT description, image, prix, poids FROM outil WHERE 1=1 ";

    if (isset($_GET['disponibleLocation']))
        $requeteSQL .= "AND disponibleLocation = ? ";
    if (isset($_GET['enLocationChez']))
        $requeteSQL .= "AND enLocationChez = ? ";
    if (isset($_GET['disponibleNotation']))
        $requeteSQL .= "AND disponibleNotation = ? ";
    if (isset($_GET['ventePossible']))
        $requeteSQL .= "AND ventePossible = ? ";
    // ...
    
    $requetePreparee = $connexion->prepare($requeteSQL);
    
    if (isset($_GET['disponibleLocation']))
        $requetePreparee->bind_param('i', $_GET['disponibleLocation']);
    if (isset($_GET['enLocationChez']))
        $requetePreparee->bind_param('s', $_GET['enLocationChez']);
    if (isset($_GET['disponibleNotation']))
        $requetePreparee->bind_param('i', $_GET['disponibleNotation']);
    if (isset($_GET['ventePossible']))
        $requetePreparee->bind_param('i', $_GET['ventePossible']);
    // ...

    return $requetePreparee;
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    if ($_GET['action'] === 'louer') {
        // ...
    } else if ($_GET['action'] === 'retouner') {
        // ...
    } else if ($_GET['action'] === 'noter') {
        // ...
    } else if ($_GET['action'] === 'vendre') {
        // ...
    }
    // ...

} else if ($_SERVER['REQUEST_METHOD'] === 'GET') {

    $connexion = creerConnexion();

    $requetePreparee = construireRequete($connexion);

    $requetePreparee->execute();
    $resultat = $requetePreparee->get_result();

    $outils = array();
    while($ligne = $resultat->fetch_assoc()) {
        $outils[] = new Outil("data:image/jpeg;base64," . $ligne["image"], $ligne["description"], $ligne["prix"], $ligne["poids"]);
    }

    $requetePreparee->close();
    $connexion->close();

    header('Content-type: application/json');
    $listeOutils = new stdClass();
    $listeOutils->outils = $outils;
    echo json_encode($listeOutils);
}

?>
